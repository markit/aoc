#!/usr/bin/env python3

import datetime
import numpy as np


log = []

for line in open('input', 'r'):
    year = int(line[3:5]) + 2000
    month = int(line[6:8])
    day = int(line[9:11])
    hour = int(line[12:14])
    minute = int(line[15:17])
    date = datetime.datetime(year, month, day, hour, minute)
    rest = line.split('] ')[1]

    if 'Guard' in rest:
        id = rest.split('#')[1].split(' ')[0]
        log.append((date, int(id)))
    elif 'up' in rest:
        log.append((date, 'awake'))
    else:
        log.append((date,'asleep'))

    log.sort(key=lambda a: a[0])

last_guard_id = None
reduced_log = []
entries = []

for date, entry in log:
    if type(entry) is int:
        reduced_log.append((last_guard_id, entries))
        last_guard_id = entry
        entries = []
    else:
        entries.append((date, entry))

guards = {}

for guard, entries in reduced_log:

    hour = guards.get(guard, np.zeros(60, np.int))
    for a, b in zip(entries, entries[1:]):
        if a[1] == 'asleep':
            ma = a[0].minute
            mb = b[0].minute
            hour[ma:mb] += 1

    guards[guard] = hour

mguard = None
msleep = 0
for guard, hour in guards.items():
    m = sum(hour)
    if msleep < m:
        msleep = m
        mguard = guard

print(mguard)

hour = guards[mguard]
print(hour.argmax())

print(hour.argmax() * mguard)

# Part 2
mguard = None
msleep = 0
for guard, hour in guards.items():
    m = max(hour)
    if msleep < m:
        msleep = m
        mguard = guard

print(mguard)

hour = guards[mguard]
print(hour.argmax())

print(hour.argmax() * mguard)
