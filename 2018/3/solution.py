#!/usr/bin/env python3

import numpy as np

fabric = np.zeros((1000, 1000), np.int)

overlap = 0
ids = set()

for line in open('input', 'r'):
    id = int(line.split('@')[0][1:])
    corner = line.split('@')[1].split(':')[0]
    corner = corner.split(',')
    corner = (int(corner[0]), int(corner[1]))
    size = line.split(':')[1]
    size = size.split('x')
    size = (int(size[0]), int(size[1]))
    ids.add(id)

    for sx in range(size[0]):
        for sy in range(size[1]):
            x = corner[0]+sx
            y = corner[1]+sy
            orig = fabric[corner[0]+sx][corner[1]+sy]
            if orig == 0:
                fabric[corner[0]+sx][corner[1]+sy] = id
            elif orig != -1:
                try:
                    ids.remove(orig)
                except:
                    pass
                try:
                    ids.remove(id)
                except:
                    pass

                fabric[corner[0]+sx][corner[1]+sy] = -1
                overlap += 1
            else:
                try:
                    ids.remove(id)
                except:
                    pass


print(overlap)
print(ids)

