#!/usr/bin/env python3

from collections import Counter

two = 0
three = 0

for line in open('input', 'r'):
    alpha = Counter()

    for char in line:
        alpha[char] += 1

    fthree = False
    ftwo = False
    for k, v in alpha.items():
        if v == 3:
            fthree = True
        elif v == 2:
            ftwo = True

    if fthree:
        three += 1
    if ftwo:
        two += 1

print(three)
print(two)
print(three * two)
