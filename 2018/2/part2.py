#!/usr/bin/env python3

import sys
from collections import Counter

for i, original in enumerate(open('input', 'r')):
    for j, line in enumerate(open('input', 'r')):
        if i == j:
            continue

        match = ''
        diff = 0
        for a, b in zip(original, line):
            if a != b:
                diff += 1
            else:
                match += a

        if diff == 1:
            print(match)
            sys.exit(0)
