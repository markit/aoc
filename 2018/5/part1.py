#!/usr/bin/env python3

polymer = ''

for poly in open('input', 'r'):
    polymer = poly

polymer = polymer[:-1]

next_polymer = []

change = True
while change:
    change = False
    for char in polymer:
        if len(next_polymer) == 0:
            next_polymer.append(char)
        else:
            last = next_polymer[-1]

            if last.upper() == char.upper() and last != char:
                change = True
                next_polymer.pop()
            else:
                next_polymer.append(char)
    polymer = next_polymer
    next_polymer = []

    print(polymer)
    print(len(polymer))
