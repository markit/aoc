#!/usr/bin/env python3

import sys

reached = set()
freq = 0
while True:
    for line in open('input', 'r'):
        if freq in reached:
            print(freq)
            sys.exit(0)
        reached.add(freq)
        freq += int(line)
