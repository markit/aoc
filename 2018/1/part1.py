#!/usr/bin/env python3

freq = 0
for line in open('input', 'r'):
    freq += int(line)

print(freq)
